<?php

   Route::group(['prefix'=>'admin-auth','namespace'=>'Admin\Auth'],
    function(){   
    Route::get('login', ['as' => 'getLoginAdmin', 'uses' => 'AdminLoginController@getLoginAdmin']);
    Route::post('login', ['as' => 'postLogin', 'uses' => 'AdminLoginController@postLogin']);
    Route::get('logout', ['as' => 'getLogout', 'uses' => 'AdminLoginController@getLogoutAdmin']);
    // Route::get('login','AdminLoginController@getLoginAdmin')->name('get.login.admin');l
    // Route::post('login','AdminLoginController@postLoginAdmin');
    // Route::get('logout','AdminLoginController@getLogoutAdmin')->name('get.logout.admin');
 });
 
 Route::group(['prefix'=>'api-admin','namespace'=>'Admin','middleware'=>'check_admin_login'], function(){
    Route::get('/', function () {
    return view('admin.index');
    });
        Route::group(['prefix'=>'menu'],function(){
        Route::get('','AdminMenuController@index')->name('admin.menu.index');
        Route::get('create','AdminMenuController@create')->name('admin.menu.create');
        Route::post('create','AdminMenuController@store');

        Route::get('update/{id}','AdminMenuController@edit')->name('admin.menu.update');
        Route::post('update/{id}','AdminMenuController@update');
        Route::get('delete/{id}','AdminMenuController@delete')->name('admin.menu.delete');
    });
        Route::group(['prefix'=>'ingredient'],function(){
        Route::get('','AdminIngredientController@index')->name('admin.ingredient.index');
        Route::get('create','AdminIngredientController@create')->name('admin.ingredient.create');
        Route::post('create','AdminIngredientController@store');

        Route::get('update/{id}','AdminIngredientController@edit')->name('admin.ingredient.update');
        Route::post('update/{id}','AdminIngredientController@update');
        Route::get('delete/{id}','AdminIngredientController@delete')->name('admin.ingredient.delete');
    });
         Route::group(['prefix' => 'user'], function(){
            Route::get('','AdminUserController@index')->name('admin.user.index');
            Route::get('create','AdminUserController@create')->name('admin.user.create');
            Route::post('create','AdminUserController@store');
            
            Route::get('update/{id}','AdminUserController@edit')->name('admin.user.update');
            Route::post('update/{id}','AdminUserController@update');

            Route::get('delete/{id}','AdminUserController@delete')->name('admin.user.delete');

        });
      // Route::group(['prefix' => 'user'], function(){
      //       Route::get('','AdminUserController@index')->name('admin.user.index');
      //       Route::get('create','AdminUserController@create')->name('admin.user.create');
      //       Route::post('create','AdminUserController@store');
            
      //       Route::get('update/{id}','AdminUserController@edit')->name('admin.user.update');
      //       Route::post('update/{id}','AdminUserController@update');

      //       Route::get('delete/{id}','AdminUserController@delete')->name('admin.user.delete');

      //   });
      //  Route::group(['prefix' => 'product'], function(){
      //       Route::get('','AdminProductController@index')->name('admin.product.index');
      //       Route::get('create','AdminProductController@create')->name('admin.product.create');
      //       Route::post('create','AdminProductController@store');
      //       Route::get('active/{id}','AdminProductController@active')->name('admin.product.active');
      //       Route::get('update/{id}','AdminProductController@edit')->name('admin.product.update');
      //       Route::post('update/{id}','AdminProductController@update');
      //       Route::get('hot/{id}','AdminProductController@hot')->name('admin.product.hot');

      //       Route::get('delete/{id}','AdminProductController@delete')->name('admin.product.delete');
      //   });


 });