<?php
use Illuminate\Http\Request;


Route::group([

     'middleware' => 'api',

], function ($router) {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login')->name('login');
    
    Route::group(['middleware' => ['jwt.verify']], function() {
    	 Route::post('refresh', 'AuthController@refresh');
    	 Route::post('me', 'AuthController@me');
    	 Route::post('logout', 'AuthController@logout');
         Route::post('postuser/{id}','DiaryController@add');

    });
    //Postuser
    Route::get('getData','DiaryController@getData');
    Route::get('getDataUserPosts/{id}','DiaryController@getDataUserPosts');
    Route::delete('postuser/{id}','DiaryController@delete');
    
    //Account
    Route::get('users','UserController@getList');
    Route::get('users/{id}','UserController@get');
    Route::put('users/{id}','UserController@edit');
    //File
    Route::post('file/uploadImage','FileController@addPostImage');
    Route::post('file/uploadVideo','FileController@addPostVideo');
    //update privacy
    Route::put('postuser/{id}','DiaryController@edit');
    //MENU
    Route::get('getDataMenuTop','MenuController@getDataMenuTop');
    Route::get('getDataMenuBottom','MenuController@getDataMenuBottom');
    Route::get('getDataMenuAll','MenuController@getDataMenuAll');
    //PLACE
    Route::get('getDataPlaceHomeRandom','PlaceController@getDataPlaceHomeRandom');
    Route::get('getDataImageHomeRandom','PlaceController@getDataImageHomeRandom');
    Route::get('getDataPlaceIdIngredient','PlaceController@getDataPlaceIdIngredient');
    //Ingredient
    Route::get('getDataIngredientIdMenu/{id}','IngredientController@getData');

});