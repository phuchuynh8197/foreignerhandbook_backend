<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_role', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('userId');
           $table->integer('roleId');
           $table->timestamps();
        //   $table->unique(['userId','roleId']);
       //    $table->foreign('userId')->references('id')->on('users')->onDelete('cascade');
       //    $table->foreign('roleId')->references('id')->on('role')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_role');
    }
}
