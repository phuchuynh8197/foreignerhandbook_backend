<?php
use App\Models\Role;
use Illuminate\Database\Seeder;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admin = Role::create([
           'id'=>1,
           'name'=>'admin',
        ]); 
        $customer = Role::create([
           'id'=>2,
           'name'=>'customer',
        ]); 
    }
}
