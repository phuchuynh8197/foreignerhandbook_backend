@extends('layouts.app_master_admin')
@section('content')
<section class="content-header">
                    <h1>
                      Cập nhập thành phần 
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="{{route('admin.ingredient.index')}}">Ingredient</a></li>
                        <li class="active">Create</a></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
          
                        </div>
                        <div class="box-body">
					     <form role="form" action ="" method="POST">
					     	  @csrf
					       <div class="col-sm-8">
					       	 <div class="form-group">
					            <label for="name">Name<span class="text-danger">(*)</span></label>
					            <input class="form-control" name="name" value="{{$ingredients->name}}" type="text" placeholder="Name ...">
					            @if ($errors->first('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
					        </div>
					       </div>
                                <div class="col-sm-8">
                                     <div class="form-group ">
                                    <label class="control-label">Menu<b class="col-red">(*)</b></label>
                                    <select name="id_menu"class="form-control ">
                                     <option  value="">__Click__</option>
                                     @foreach($menus as $menu)
			                         <option value="{{$menu->id}}" {{($ingredients->id_menu ?? 0) == $menu->id ? "selected='selected'" : ""}}>{{$menu->name}}
			                         </option>
			                    	@endforeach
                                         <!-- <option value="0">Admin</option>
                                         <option value="1">Customer</option> -->
                                    </select>
                                     @if ($errors->first('id_menu'))
                                        <span class="text-danger">{{ $errors->first('id_menu')}}</span>
                                    @endif
                                </div>
                                </div>
					       <div class="col-sm-4">
					       	   
					       </div>
					       <div class="col-sm-12">
					       	<div class="box-footer text-center">
                                <button class="btn btn-success" type="submit">Lưu dữ liệu</button>
                                <a href="{{route('admin.menu.index')}}" class="btn btn-danger">Quay lại</a>
                           </div>
					       </div>
					</form>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            Footer
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </section>
                @stop