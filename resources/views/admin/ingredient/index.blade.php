@extends('layouts.app_master_admin')
@section('content')
<section class="content-header">
                    <h1>
                      Quản lý ingredient ứng dụng 
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="<!-- {{route('admin.ingredient.index')}} -->"></a></li>
                        <li class="active">List</a></li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">
                        	 <div class="box-header">
                    <h3 class="box-title"><a href="{{route('admin.ingredient.create')}}" class="btn btn-primary">Thêm mới <i class="fa fa-plus"></i></a></h3>
               </div>
                        </div>
                        <div class="box-body">
                          <div class="col-md-12">
                     <table class="table">
                        <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                     @if($ingredients)
                     @foreach($ingredients as $ingredient)
                      <tr>
                        <td>{{$ingredient->id}}</td>
                        <td>{{$ingredient->name}}</td>
                         <td>{{$ingredient->created_at}}</td>
                        <td>
                            <a href="{{route('admin.ingredient.update', $ingredient->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="{{route('admin.ingredient.delete',$ingredient->id)}}"class = "btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                     @endforeach
                     @endif
                </tbody>
            </table>
    <!-- /.box -->
                 </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                           {!! $ingredients->links() !!}
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->
                </section>
                @stop