<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
class MenuController extends Controller
{
    //
    public function getDataMenuTop()
    {
    	$table = Menu::where('id_table','=',0)->get();
    	return $this->respondData($table,$table->count());
    }


    public function getDataMenuBottom()
    {
       $table = Menu::where('id_table','=',1)->get();
       return $this->respondData($table,$table->count());
    }
    public function getDataMenuAll()
    {
    	$table = Menu::all();
    	return $this->respondData($table,$table->count());
    }

    protected function respondData($data,$total)
    {
    	return response()->json([
           'statusCode'=>200,
           'errorMessage'=>'',
           'data'=>$data,
           'total'=>$total,
 
    	]);
    }
    
}
