<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Library\MyValidation;
use Validator;
class AuthController extends Controller
{
      
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'status_code' => 202,
                'message' => 'Email or password is incorrect'
            ], 202);
        }
       $user = User::where('Email', $request['email'])->with('roles')->first();
        return $this->respondWithToken($token,$user);
    }
   
    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), MyValidation::$rulesUser, MyValidation::$messageUser);

        if ($validator->fails()) {
            $message = $validator->messages()->getMessages();
            return response()->json(['message'=>$message, 'status_code' => 202], 202);
        }
        $data = $request->all();
        $user = User::create($data);
        $token = auth()->login($user);
        return $this->respondWithToken($token, $user);
    }

    protected function respondWithToken($token, $user)
    {
        return response()->json([
            'statusCode' => 200,
            'errorMessage' => '',
            'data'=>[
                'token'=>$token,
                'profile'=>$user,
            ]
        ], 200);
    }
 }
