<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Menu;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\AdminRequestMenu;

class AdminMenuController extends Controller
{
    //
    public function index()
    {
    	$menus = Menu::paginate(5);
    	$viewData = ['menus'=>$menus];
    	return view('admin.menu.index',$viewData);
    }

    public function create()
    {
    	return view('admin.menu.create');
    }
    public function store(AdminRequestMenu $request)
    {
        $data = $request->except('_token','icons');
        $data['m_slug'] = Str::slug($request->name);
        $data['created_at'] = Carbon::now();
        if($request->icons){
         $image = upload_image('icons');
       //dd($image);
        //dd($image);
         if ($image['code'] == 1) 
             $data['icons'] = $image['name'];
       }
        $id = Menu::insertGetId($data);
        return redirect()->back();
    }
    public function edit($id)
    {
         $menu = Menu::find($id);
         return view('admin.menu.update',compact('menu'));
    }
    public function update(AdminRequestMenu $request,$id)
    {
        $menu = Menu::find($id);
        $data = $request->except('_token','icons');
        $data['m_slug'] = Str::slug($request->name);
        $menu->update($data);
        return redirect()->back();
    }
    public function delete($id)
    {
        $menu = Menu::find($id);
        $menu->delete($menu);
        return redirect()->route('admin.menu.index');
    }

    
}