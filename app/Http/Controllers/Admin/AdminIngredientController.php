<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Models\Menu;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\AdminRequestIngredient;

class AdminIngredientController extends Controller
{
    //
    public function index()
    {
      $ingredients = Ingredient::paginate(5);
      $viewData = ['ingredients'=>$ingredients];
      return view('admin.ingredient.index',$viewData);
    }
    public function create()
    {
    	$menus = Menu::all();
    	return view('admin.ingredient.create',compact('menus'));
    }
    public function store(AdminRequestIngredient $request)
    { 
         $data = $request->except('_token');
        //dd($data);
        $data['ingre_slug'] = Str::slug($request->name,'_');
        $data['created_at'] = Carbon::now();
     //  dd($data);
        $id = Ingredient::insertGetId($data);
        return redirect()->back();
    }
    public function edit($id)
    {
        $menus = Menu::all();
        $ingredients = Ingredient::find($id);
        return view('admin.ingredient.update',compact('menus','ingredients'));
    }
    public function update(AdminRequestIngredient $request,$id)
    {
        $ingredients = Ingredient::find($id);
        $data = $request->except('_token');
        $data['ingre_slug'] = Str::slug($request->name);
        $data['updated_at'] = Carbon::now();
        $ingredients->update($data);
        return redirect()->route('admin.ingredient.index');
    }
    public function delete($id)
    {
        $ingredient = Ingredient::find($id);
        if($ingredient){
            $ingredient->delete($ingredient);
        }
        return redirect()->route('admin.ingredient.index');
    }
}
