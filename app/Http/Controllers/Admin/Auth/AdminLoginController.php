<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\LoginRequest;
class AdminLoginController extends Controller
{
    //
   // use AuthenticatesUsers;

    public function getLoginAdmin()
    {   
        if(\Auth::check()){
            return redirect('api-admin');
        }
    	return view('admin.auth.login');
    }
    public function postLogin(LoginRequest $request)
    {
       $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 1,
        ];
       // dd($login);
        if (\Auth::attempt($login)) {
             return redirect()->intended('/api-admin');
        } else {
            return redirect()->back()->with('status', 'Email hoặc Password không chính xác');
        }
    }
    public function getLogoutAdmin()
    {
    	\Auth::logout();
        return redirect()->route('getLoginAdmin');
    }
}
