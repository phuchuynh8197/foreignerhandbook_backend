<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AdminRequestUser;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\AdminR;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class AdminUserController extends Controller
{
    //
    public function index()
    {
    	$users  = User::paginate(2);
    	$viewData = [
         'users'=> $users
    	];
    	return view('admin.user.index',$viewData);
    }
    public function create()
    {
    	$users = User::all();
    	return view('admin.user.create',compact('users'));
    }
     public function store(AdminRequestUser $request)
    {
       $data = $request->except('_token','avatar');
       $data['user_slug'] =  Str::slug($request->name);
       $data['created_at'] = Carbon::now(); 
       $password = Hash::make($request->password);
       $data['password'] = $password;
       if($request->avatar){
    	 $image = upload_image('avatar');
       //dd($image);
    	//dd($image);
    	 if ($image['code'] == 1) 
           $data['avatar'] = $image['name'];
       }
       $id = User::insertGetId($data);
       return redirect()->back();
       dd($data);    
   }

   public function edit($id)
   {
        $user = User::findOrFail($id);
        return view('admin.user.update',compact('user'));
   }
   public function update(AdminRequestUser $request, $id)
   {
     $user = User::findOrFail($id);
     $data = $request->except('_token','avatar');
     $data['user_slug'] = Str::slug($request->name);
     $data['updated_at'] = Carbon::now();

     if($request->avatar){
    	$image = upload_image('avatar');
    	//dd($image);
    	 if ($image['code'] == 1) 
              $data['avatar'] = $image['name'];
    }
     $user->update($data);
     return redirect()->route('admin.user.index');
   }
   public function delete($id)
   {
   	$user = User::find($id);
   	if($user){
   		$user->delete($user);
   		
   		return redirect()->route('admin.user.index');
   	}
   }
}
