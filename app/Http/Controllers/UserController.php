<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserController extends Controller
{
    //
     public function getList()
     {
        $table = User::all();
        return $this->respondData($table);
     }
     public function get(Request $request, $id)
     {
        $table = User::where('id','=',$id)->get();

        return $this->respondData($table);
     }
     public function edit(Request $request , $id)
     {
        $table = User::findOrFail($id);
        if($table){
            $editUser['name'] = $request->input('name');
            $editUser['phone'] = $request->input('phone');
            $editUser['age'] = $request->input('age');
            $editUser['gender'] = $request->input('gender');
           $table->update($editUser);
            return $this->respondData($table);
        }  
     }

    protected function respondData($table)
    {
        return response()->json([
            'message' => 'Successfully',
            'statuscode' => '200',
            'data' => $table,
            'total' => $table->count(),
        ]);
    }
}
