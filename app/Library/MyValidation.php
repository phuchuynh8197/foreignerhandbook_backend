<?php
namespace App\Library;

class MyValidation
{

    public static $rulesUser = array(
        // 'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|alpha_num|min:3|max:20',
        // 'avatarUrl' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    );

    public static $ruleSkill = array(
        'name' => 'required|unique:skill',
    );

    public static $ruleQuestion = array(
        'question_content' => 'required',
    );

    public static $ruleDoctor = array(
        'name' => 'required',
        'id_number' => 'required|unique:doctors',
        'email' => 'required|unique:doctors',
    );

    public static $messageSkill = array(
        'name.required' => 'Question content is required',
    );

    public static $messageUser = array(
        // 'name' => 'Name is missing',
        'email.required' => 'Email is missing',
        'email.email' => 'Email is incorrect',
        'password.required' => 'Password is missing',
        'password.min' => 'Minimum length of password must be more than 3',
        'password.max' => 'Maximum length of password must be less than 20',
        'password.alpha_num' => 'Password should contain both number and character',
        // 'avatarUrl.image'=>'Avatar must be image',
        // 'avatarUrl.mimes'=>'Avatar must be incorect type',
        // 'avatarUrl.max'=>'Avatar max size 2048 kb',

    );

    public static $messageQuestion = array(
        'question_content' => 'Question content is required',
    );


    public static $messageDoctor = array(
        'name' => 'Name is required',
        'email' => 'Email is required'
    );
    
}
