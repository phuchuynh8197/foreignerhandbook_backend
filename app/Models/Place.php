<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    //
    protected $table = 'place';
    protected $fillable =[
     'id',
     'name',
     'image',
     'introduce',
     'overview',
     'arrayImageView',
     'id_ingredient',
    ];
    protected $hidden = ['created_at','updated_at'];
}
