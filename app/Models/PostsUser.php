<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostsUser extends Model
{
    //
    protected $table = "postsuser";

   // public $timestamps = true;

    public $incrementing = false; 

    protected $fillable = [
      'id',
      'id_user',
      'image',
      'content',
      'privacy',
    //  'created_at',
    ];
    //protected $hidden = ['created_at', 'updated_at'];
    public function users()
    {
    	return $this->belongsto('App\Models\User');
    }
}
